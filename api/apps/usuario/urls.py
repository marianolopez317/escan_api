from __future__ import absolute_import
from django.conf.urls import url
from apps.usuario.views import UsuarioAPI,UsuarioIdAPI,UsuarioLoginAPI,HistorialAPI,HistorialIdAPI,CuidadoAPI,CuidadoIdAPI,AlimentacionAPI,AlimentacionIdAPI,DatoCuriosoAPI,DatoCuriosoIdAPI,RazaAPI,RazaIdAPI,ClasificarAPI

urlpatterns=[
    url(r'^$',UsuarioAPI.as_view()),
    url(r'^/(?P<id_usuario>\d+)$',UsuarioIdAPI.as_view()),
    url(r'^/login$',UsuarioLoginAPI.as_view()),
    url(r'^/historial$',HistorialAPI.as_view()),
    url(r'^/clasificar$',ClasificarAPI.as_view()),
    url(r'^/historial/(?P<id_usuario>\d+)$',HistorialIdAPI.as_view()),
    url(r'^/cuidado$',CuidadoAPI.as_view()),
    url(r'^/cuidado/(?P<id_raza>\d+)$', CuidadoIdAPI.as_view()),
    url(r'^/alimentacion$',AlimentacionAPI.as_view()),
    url(r'^/alimentacion/(?P<id_raza>\d+)$', AlimentacionIdAPI.as_view()),
    url(r'^/datoCurioso$',DatoCuriosoAPI.as_view()),
    url(r'^/datoCurioso/(?P<id_raza>\d+)$', DatoCuriosoIdAPI.as_view()),
    url(r'^/raza$',RazaAPI.as_view()),
    url(r'^/raza/(?P<id_raza>\d+)$', RazaIdAPI.as_view()),


]