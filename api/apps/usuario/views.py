# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import absolute_import
import json
from rest_framework.views import APIView
#from django.shortcuts import render
from django.http import HttpResponse,Http404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
#from django.core import serializers
from apps.usuario.models import Usuario,Historial,Cuidado,DatoCurioso,Alimentacion,Raza
from apps.usuario.serializers import UsuarioSerializer, HistorialSerializer,CuidadoSerializer,DatoCuriosoSerializer,AlimentacionSerializer,RazaSerializer
from random import randint
# Create your views here.

#def index(request):
    #return HttpResponse("<h2>Usuario</h2>")
    #usuario= Usuario.objects.all()
    #if request.GET:
    #data=serializers.serialize('json',Usuario.objects.all(),fields=['nombre','apPaterno','apMaterno','correo','fechaNacimiento','sexo'])
    #data={
    #    'nombre':'mariano',
    #    'apPaterno': 'lopez china',
    #    'apMaterno': 'santiago',
    #}
    #return HttpResponse(data, content_type='application/json')
    #else:
        #return HttpResponse(usuario)
#@csrf_exempt
@method_decorator(csrf_exempt, name='dispatch')
class UsuarioLoginAPI(APIView):
    serializer=UsuarioSerializer
    def post(self,request,format=None):
        correo=request.POST.get('correo')
        contrasenia=request.POST.get('contrasenia')
        if(correo) and (contrasenia):
            if(Usuario.objects.filter(correo=correo,contrasenia=contrasenia)):
                lista=Usuario.objects.get(correo=correo)
                respuesta = {
                    'status': '1',
                    'mensaje': lista.idUsuario
                }
                return HttpResponse(json.dumps(respuesta), content_type='application/json')
            else:
                respuesta = {
                    'status': '0',
                    'mensaje': 'Usuario o contrasenia incorrecta'
                }
                return HttpResponse(json.dumps(respuesta), content_type='application/json')
        else:
            respuesta={
                    'status':'0',
                    'mensaje':'parametros nullos'
            }
            return HttpResponse(json.dumps(respuesta), content_type='application/json')



class UsuarioAPI(APIView):
    serializer=UsuarioSerializer
    def get(self,request,format=None):
        lista=Usuario.objects.all()
        response=self.serializer(lista,many=True)
        return HttpResponse(json.dumps(response.data),content_type='application/json')
    def post(self,request,format=None):
        nombre=request.POST.get('nombre')
        apPaterno = request.POST.get('apPaterno')
        apMaterno = request.POST.get('apMaterno')
        correo=request.POST.get('correo')
        contrasenia=request.POST.get('contrasenia')
        fechaNacimiento = request.POST.get('fechaNacimiento')
        tipoUsuario = '0'
        sexo=request.POST.get('sexo')
        if (correo) and (contrasenia) and (nombre) and (apPaterno) and (apMaterno) and (sexo) and (fechaNacimiento):
            u=Usuario(nombre=nombre,apPaterno=apPaterno,apMaterno=apMaterno,correo=correo,contrasenia=contrasenia,sexo=sexo,fechaNacimiento=fechaNacimiento,tipoUsuario=tipoUsuario)
            u.save()
            respuesta = {
                'status': '1',
                'mensaje': 'ok'
            }
            return HttpResponse(json.dumps(respuesta), content_type='application/json')
        else:
            respuesta = {
                'status': '0',
                'mensaje': 'hay parametros nullos'
            }
            return HttpResponse(json.dumps(respuesta), content_type='application/json')






class UsuarioIdAPI(APIView):
    serializer=UsuarioSerializer
    def get(self,request,id_usuario,format=None):
        #user_id = request.GET['user_id']
        if(Usuario.objects.filter(idUsuario=id_usuario)):
            lista=Usuario.objects.get(idUsuario=id_usuario)
            response=self.serializer(lista)
            return HttpResponse(json.dumps(response.data),content_type='application/json')
        else:
            #return HttpResponse(json.dumps({"error":"404"}), content_type='application/json')
            raise Http404("it does not exist")
    def put(self, request,id_usuario, format=None):
        nombre = request.POST.get('nombre')
        apPaterno = request.POST.get('apPaterno')
        apMaterno = request.POST.get('apMaterno')
        correo = request.POST.get('correo')
        contrasenia = request.POST.get('contrasenia')
        fechaNacimiento = request.POST.get('fechaNacimiento')
        sexo = request.POST.get('sexo')
        if (correo) and (contrasenia) and (nombre) and (apPaterno) and (apMaterno) and (sexo) and (fechaNacimiento):
            u=Usuario.objects.get(idUsuario=id_usuario)
            u.nombre=nombre
            u.apPaterno=apPaterno
            u.apMaterno=apMaterno
            u.correo=correo
            u.contrasenia=contrasenia
            u.sexo=sexo
            u.fechaNacimiento=fechaNacimiento
            u.save()
            respuesta = {
                'status': '1',
                'mensaje': 'ok'
            }
            return HttpResponse(json.dumps(respuesta), content_type='application/json')
        else:
            respuesta = {
                'status': '0',
                'mensaje': 'hay parametros nullos'
            }
            return HttpResponse(json.dumps(respuesta), content_type='application/json')


class HistorialAPI(APIView):
    serializer=HistorialSerializer
    def get(self,request,format=None):
        lista=Historial.objects.all()
        response=self.serializer(lista,many=True)
        return HttpResponse(json.dumps(response.data),content_type='application/json')
    def post(self,request,format=None):
        idUsuario=request.POST.get('idUsuario')
        idRaza = request.POST.get('idRaza')

        if (idUsuario) and (idRaza) :
            h=Historial(idUsuario=idUsuario,idRaza=idRaza)
            h.save()
            respuesta = {
                'status': '1',
                'mensaje': 'ok'
            }
            return HttpResponse(json.dumps(respuesta), content_type='application/json')
        else:
            respuesta = {
                'status': '0',
                'mensaje': 'hay parametros nullos'
            }
            return HttpResponse(json.dumps(respuesta), content_type='application/json')

class HistorialIdAPI(APIView):
    serializer=HistorialSerializer
    def get(self,request,id_usuario,format=None):
        lista=Historial.objects.filter(idUsuario=id_usuario)
        response=self.serializer(lista,many=True)
        return HttpResponse(json.dumps(response.data),content_type='application/json')

class ClasificarAPI(APIView):
    def post(self,request,format=None):
        data = json.loads(request.body)
        r = randint(1, 8)
        h=Historial(idUsuario=Usuario.objects.get(idUsuario=data['idUsuario']),idRaza=Raza.objects.get(idRaza=r),imagen=data['imagen'])
        h.save()
        return HttpResponse(json.dumps({"resultado":r}),content_type='application/json')


#class ClasificarAPI(APIView):
    #def post(self,request,format=None):
        #idUsuario = request.POST.get('idUsuario')
        #imagen = request.POST.get('imagen')
        #imagen.rstrip('\n')
        #r = randint(1, 8)
        #h=Historial(idUsuario=Usuario.objects.get(idUsuario=idUsuario),idRaza=Raza.objects.get(idRaza=r),imagen=imagen)
        #h.save()
        #r=str(r)
        #return HttpResponse(json.dumps({"resultado":r}),content_type='application/json')


class CuidadoAPI(APIView):
    serializer=CuidadoSerializer
    def get(self,request,format=None):
        lista =Cuidado.objects.all()
        response = self.serializer(lista, many=True)
        return HttpResponse(json.dumps(response.data), content_type='application/json')

class CuidadoIdAPI(APIView):
    serializer=CuidadoSerializer
    def get(self,request,id_raza,format=None):
        lista=Cuidado.objects.filter(idRaza=id_raza)
        response=self.serializer(lista, many=True)
        return HttpResponse(json.dumps(response.data),content_type='application/json')




class AlimentacionAPI(APIView):
    serializer=AlimentacionSerializer
    def get(self,request,format=None):
        lista =Alimentacion.objects.all()
        print lista
        response = self.serializer(lista, many=True)
        return HttpResponse(json.dumps(response.data), content_type='application/json')

class AlimentacionIdAPI(APIView):
    serializer=AlimentacionSerializer
    def get(self,request,id_raza,format=None):
        lista=Alimentacion.objects.filter(idRaza=id_raza)
        response = self.serializer(lista, many=True)
        return HttpResponse(json.dumps(response.data),content_type='application/json')




class DatoCuriosoAPI(APIView):
    serializer=DatoCuriosoSerializer
    def get(self,request,format=None):
        lista =DatoCurioso.objects.all()
        response = self.serializer(lista, many=True)
        return HttpResponse(json.dumps(response.data), content_type='application/json')

class DatoCuriosoIdAPI(APIView):
    serializer=DatoCuriosoSerializer
    def get(self,request,id_raza,format=None):
        lista=DatoCurioso.objects.filter(idRaza=id_raza)
        response=self.serializer(lista, many=True)
        return HttpResponse(json.dumps(response.data),content_type='application/json')




class RazaAPI(APIView):
    serializer=RazaSerializer
    def get(self,request,format=None):
        lista =Raza.objects.all()
        response = self.serializer(lista, many=True)
        return HttpResponse(json.dumps(response.data), content_type='application/json')

class RazaIdAPI(APIView):
    serializer=RazaSerializer
    def get(self,request,id_raza,format=None):
        if(Raza.objects.filter(idRaza=id_raza)):
            lista=Raza.objects.get(idRaza=id_raza)
            response=self.serializer(lista)
            return HttpResponse(json.dumps(response.data),content_type='application/json')
        else:
            raise Http404("it does not exist")