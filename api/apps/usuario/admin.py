# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import absolute_import

from django.contrib import admin

from apps.usuario.models import Usuario,Alimentacion,Historial,DatoCurioso,Cuidado,Raza

# Register your models here.

admin.site.register(Usuario)
admin.site.register(Raza)
admin.site.register(Historial)
admin.site.register(Cuidado)
admin.site.register(Alimentacion)
admin.site.register(DatoCurioso)

