from __future__ import absolute_import
from rest_framework.serializers import ModelSerializer
from apps.usuario.models import Usuario,Historial,Cuidado,Alimentacion,DatoCurioso,Raza

class UsuarioSerializer(ModelSerializer):
    class Meta:
        model=Usuario
        fields=('nombre','apPaterno','apMaterno','correo','fechaNacimiento','sexo','contrasenia')

class HistorialSerializer(ModelSerializer):
    class Meta:
        model=Historial
        fields=('idUsuario','idRaza','fecha')


class CuidadoSerializer(ModelSerializer):
    class Meta:
        model=Cuidado
        fields=('idCuidado','pelo','higiene','vacuna','enfermedad')


class AlimentacionSerializer(ModelSerializer):
    class Meta:
        model=Alimentacion
        fields=('idAlimentacion','tipo','porcion','frecuencia')


class DatoCuriosoSerializer(ModelSerializer):
    class Meta:
        model=DatoCurioso
        fields=('idDatoCurioso','datoCurioso')


class RazaSerializer(ModelSerializer):
    class Meta:
        model=Raza
        fields=('idRaza','nombre','origen','uso','esperanzaVida')