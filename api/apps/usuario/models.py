# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Usuario(models.Model):
    idUsuario=models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=25)
    apPaterno=models.CharField(max_length=25)
    apMaterno=models.CharField(max_length=25)
    correo=models.EmailField()
    tipoUsuario=models.IntegerField()
    fechaNacimiento=models.DateField()
    masculino='m'
    femenino='f'
    sexo_choices=(
        (masculino,'Masculino'),(femenino,'Femenino')
    )
    sexo=models.CharField(
        max_length=1,
        choices=sexo_choices
    )
    contrasenia=models.CharField(max_length=50)

    def __unicode__(self):
        return '{} {} {} {} {} {} {} {}'.format(self.nombre, self.apPaterno, self.apMaterno,self.correo,self.tipoUsuario,self.fechaNacimiento,self.sexo,self.contrasenia)

    class Meta:
        ordering = ('idUsuario',)

class Raza(models.Model):
    idRaza = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    origen = models.CharField(max_length=100,null=True, blank=True)
    uso= models.CharField(max_length=100,null=True, blank=True)
    esperanzaVida= models.CharField(max_length=50,null=True, blank=True)
    idUsuario=models.ManyToManyField(Usuario,through='Historial')

    def __unicode__(self):
        return '{} {} {} {} {}'.format(self.idRaza,self.nombre,self.origen,self.uso,self.esperanzaVida)

    class Meta:
        ordering = ('idRaza',)

class Historial(models.Model):
    idUsuario=models.ForeignKey(Usuario)
    idRaza=models.ForeignKey(Raza)
    fecha=models.DateTimeField(auto_now_add=True)
    imagen=models.BinaryField()
    resultadoCorrecto=models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return '{} {} {}'.format(self.idUsuario,self.idRaza,self.fecha)

    class Meta:
        ordering = ('fecha',)

class Alimentacion(models.Model):
    idAlimentacion = models.AutoField(primary_key=True)
    tipo = models.CharField(max_length=1000,null=True, blank=True)
    porcion = models.CharField(max_length=1000,null=True, blank=True)
    frecuencia = models.CharField(max_length=1000,null=True, blank=True)
    idRaza=models.ForeignKey(Raza, null=False, blank=False, on_delete=models.CASCADE)
    def __unicode__(self):
        return '{} {} {} {} {}'.format(self.idAlimentacion,self.tipo,self.porcion,self.frecuencia,self.frecuencia,self.idRaza)

    class Meta:
        ordering = ('idRaza',)

class Cuidado(models.Model):
    idCuidado=models.AutoField(primary_key=True)
    pelo=models.CharField(max_length=1000,null=True, blank=True)
    higiene = models.CharField(max_length=1000,null=True, blank=True)
    vacuna = models.CharField(max_length=1000,null=True, blank=True)
    enfermedad = models.CharField(max_length=1000,null=True, blank=True)
    idRaza = models.ForeignKey(Raza, null=False, blank=False, on_delete=models.CASCADE)

    def __unicode__(self):
        return '{} {} {} {} {}'.format(self.idCuidado,self.pelo,self.higiene,self.vacuna,self.enfermedad,self.idRaza)

    class Meta:
        ordering = ('idRaza',)

class DatoCurioso(models.Model):
    idDatoCurioso = models.AutoField(primary_key=True)
    datoCurioso = models.CharField(max_length=1000, null=True, blank=True)
    idRaza = models.ForeignKey(Raza, null=False, blank=False, on_delete=models.CASCADE)

    def __unicode__(self):
        return '{} {} {}'.format(self.idDatoCurioso,self.datoCurioso,self.idRaza)

    class Meta:
        ordering = ('idRaza',)


